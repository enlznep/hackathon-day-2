Django Boilerplate
==================

A base template for Django projects. Includes commonly used Django 3rd party apps, admin enhancements as well as helpful bash scripts.

- Django v1.8.3
- Bootstrap v3.3.5
- jQuery v2.1.4

Included 3rd-party apps:
------------------------

    django-bootstrap-form==3.2
    django-braces==1.8.1
    django-flat-theme==0.9.5
    easy-thumbnails==2.2

Setup:
------

    1. update ./requirements.txt change `base` to [ local, test, or prod ]

    2. update PROJECT_NAME on the bin/*.sh files

    3. cp ./conf/local_settings.tpl ./conf/local_settings.py

Bash Scripts
------------

    1. ./bin/install_os_dependencies.sh

    2. ./bin/check_python_dependencies.sh

    3. ./bin/prepare_local_environment.sh

    4. ./bin/sync_dev.sh

Contribution
------------

Feel free to issue pull requests guys! :)


STRAIGHT FORWARD SETUP:

FOLDER STRUCTURE:

 - hackathon-day-2 # PROJECT_ROOT
    - assets
    - bin
    - conf
    - docs
    - requirements
 - env
 - logs


1.) Clone Repo

    - git clone git@bitbucket.org:enlznep/hackathon-day-2.git

2.) Create Virtualenv

    - virtualenv env

3.) Activate Virtual Environment

    - source env/bin/activate

4.) Create logs folder

    - mkdir logs

5.) Install Requirements

    NOTE: MAKE SURE YOU ARE IN PROJECT_ROOT (CHECK FOLDER STRUCTURE ABOVE)

    5.1) Open requirements.txt
          - change -r requirements/base.txt to -r requirements/local.txt
          - save it

    5.2)
        - pip install -r requirements.txt

6.) Sync Database

    NOTE: MAKE SURE YOU ARE IN PROJECT_ROOT (CHECK FOLDER STRUCTURE ABOVE)

    - python manage.py syncdb

7.) Copy local_settings.py

    - cp ./conf/local_settings.tpl ./conf/local_settings.py

8.) Runserver

    - python manage.py runserver




